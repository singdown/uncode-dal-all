package cn.uncode.dal.datasource;

public class DBContextHolder {
	
	public static final String WRITE = "write";
	public static final String READ = "read";
	public static final String STANDBY = "standby";
	public static final String REPORT = "report";
	
	private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<String>();
	private static final ThreadLocal<Boolean> CONTEXT_SIGN = new ThreadLocal<Boolean>();

	public static void swithToWrite() {
		Boolean flag = CONTEXT_SIGN.get();
		if(flag == null || false){
			CONTEXT_HOLDER.set(WRITE);
		}
	}
	
	public static void swithToRead() {
		Boolean flag = CONTEXT_SIGN.get();
		if(flag == null || false){
			CONTEXT_HOLDER.set(READ);
		}
	}
	
	public static void swithTo(String dbType) {
		CONTEXT_SIGN.set(true);
		CONTEXT_HOLDER.set(dbType);
	}

	public static String getCurrentDataSourceKey() {
		return CONTEXT_HOLDER.get();
	}

	public static void clear() {
		CONTEXT_HOLDER.remove();
	}
	
}
