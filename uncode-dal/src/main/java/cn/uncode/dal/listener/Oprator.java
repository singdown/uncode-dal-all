package cn.uncode.dal.listener;


/**
 * 
 * @author juny.ye
 * @email  juny.ye@ksudi.com
 *
 * 2015年4月23日
 */
public enum Oprator {
	
	GET("GET"), INSERT("INSERT"), UPDATE("UPDATE"), DELETE("DELETE"), COUNT("COUNT"), LIST("LIST"), PAGE("PAGE");
	
	public final String type;

	Oprator(String type) {
		this.type = type;
	}

}
