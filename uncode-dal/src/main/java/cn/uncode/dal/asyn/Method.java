package cn.uncode.dal.asyn;

public enum Method {
	
	INSERT(1), INSERT_TABLE(2), INSERT_DATABASE_TABLE(3);

	public final int type;

	Method(int type) {
		this.type = type;
	}

}
